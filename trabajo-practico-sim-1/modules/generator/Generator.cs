﻿using System.Collections.Generic;

namespace trabajo_practico_sim_1.modules.generator
{
    public class Generator
    {
        public long m;
        public long a;
        public long c;
        public long _last;
        public long counter;

        public Generator(long seed, long m_value, long a_value, long c_value)
        {
            a = a_value;
            m = m_value;
            c = c_value;
            _last = seed;
        }

        public Generator()
        {

        }


        public void SetVariables(long seed, long m_value, long a_value, long c_value)
        {
            a = a_value;
            m = m_value;
            c = c_value;
            _last = seed;
        }

        public long Next()
        {
            _last = ((a * _last) + c) % m;

            return _last;
        }

        public long Next(long maxValue)
        {
            return Next() % maxValue;
        }

        public IEnumerable<long[]> GetSet(long bound)
        {
            for (long i = 1; i < bound; i++)
            {
                long[] next = new long[] { i, Next() };
                yield return next;
            }
        }

        public IEnumerable<double> GetRandoms(long bound)
        {
            for (long i = 0; i < bound; i++)
            {
                decimal r = (decimal)Next() / decimal.Parse(this.m.ToString());
                yield return (double) r;
            }
        }
    }

}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace trabajo_practico_sim_1.modules
{
    public class Interval
    {
        public float lower_bound { get; set; }
        public float upper_bound { get; set; }
        public float expected_freq { get; set; }
        public float observed_freq { get; set; }
        public float chi_squared { get; set; }
        public Interval()
        {
            this.observed_freq = 0;
        }

        public override string ToString()
        {
            return String.Format("\n{0}-{1}: \n EF-> {2} \n OF-> {3} \n Chi-Sq-> {4}",
                this.lower_bound.ToString("0.0000"), this.upper_bound.ToString("0.0000"),
                this.expected_freq, this.observed_freq, this.chi_squared.ToString("0.0000"));
        }

        public string GetIntervalBounds()
        {
            return String.Format("{0}-{1}", this.lower_bound.ToString("0.0000"),
                this.upper_bound.ToString("0.0000"));
        }
    }
}

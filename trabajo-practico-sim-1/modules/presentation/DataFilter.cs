﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace trabajo_practico_sim_1.modules
{

    class DataFilter
    {

        private IEnumerable<long[]> data;

        public IEnumerable<long[]> Data
        {
            set { this.data = value; }
        }

        public IEnumerable<long[]> GetDataInRange(long lower_bound, long upper_bound)
        {
            return data.Where(x => x[0] >= lower_bound && x[0] <= upper_bound);
        }


        public static bool ValidateDataInput(Dictionary<string, string> keyValuePairs)
        {
            long result;
            bool valid = true;
            foreach (KeyValuePair<string, string> kvp in keyValuePairs)
            {
                if (long.TryParse(kvp.Value, out result) && (result >= 0))
                {
                    Console.WriteLine("Key: {0}, Value: {1}", kvp.Key, kvp.Value);
                }
                else
                {
                    valid = false;
                    MessageBox.Show(String.Format("Ingrese un valor numerico positivo para: {0}", kvp.Key));
                    break;
                }
            }
            return valid;
        }


        //var watch = new System.Diagnostics.Stopwatch();

        //watch.Start();

        //    IEnumerable<long[]> gen = rng.GetSet(20);

        ////var test = gen.Where(x => x[0] <= 1000010 && x[0] >= 1000000 );

        //watch.Stop();

        //    Console.WriteLine($"Execution Time: {watch.ElapsedMilliseconds} ms");

        //    Console.WriteLine("Done");



    }
}

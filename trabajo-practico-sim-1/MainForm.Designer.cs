﻿namespace trabajo_practico_sim_1
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea5 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend5 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series9 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series10 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Title title13 = new System.Windows.Forms.DataVisualization.Charting.Title();
            System.Windows.Forms.DataVisualization.Charting.Title title14 = new System.Windows.Forms.DataVisualization.Charting.Title();
            System.Windows.Forms.DataVisualization.Charting.Title title15 = new System.Windows.Forms.DataVisualization.Charting.Title();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            this.initTab = new System.Windows.Forms.TabControl();
            this.tabAleatorios = new System.Windows.Forms.TabPage();
            this.chkMultiplicativo = new System.Windows.Forms.CheckBox();
            this.btnNuevoRandom = new System.Windows.Forms.Button();
            this.dgvAleatorios = new System.Windows.Forms.DataGridView();
            this.clmIteracion = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.clmX = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.clmR = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.btnLimpiar = new System.Windows.Forms.Button();
            this.btnGenerar = new System.Windows.Forms.Button();
            this.txtIncremento = new System.Windows.Forms.TextBox();
            this.lblIncremento = new System.Windows.Forms.Label();
            this.txtModulo = new System.Windows.Forms.TextBox();
            this.lblModulo = new System.Windows.Forms.Label();
            this.txtMultiplicador = new System.Windows.Forms.TextBox();
            this.lblMultiplicador = new System.Windows.Forms.Label();
            this.txtSemilla = new System.Windows.Forms.TextBox();
            this.lblSemilla = new System.Windows.Forms.Label();
            this.tabChiCuadrado = new System.Windows.Forms.TabPage();
            this.lblSumatoriaResultado = new System.Windows.Forms.Label();
            this.lblSumatoria = new System.Windows.Forms.Label();
            this.chtChiFrecuencias = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.dgvChi = new System.Windows.Forms.DataGridView();
            this.idRow = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.interval = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.obs_freq = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.exp_freq = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.chi_param = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.btnGenerarChi = new System.Windows.Forms.Button();
            this.txtNumeros = new System.Windows.Forms.TextBox();
            this.lblNumeros = new System.Windows.Forms.Label();
            this.txtIntervalos = new System.Windows.Forms.TextBox();
            this.lblIntervalos = new System.Windows.Forms.Label();
            this.chkMixto = new System.Windows.Forms.CheckBox();
            this.chkExportar = new System.Windows.Forms.CheckBox();
            this.initTab.SuspendLayout();
            this.tabAleatorios.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvAleatorios)).BeginInit();
            this.tabChiCuadrado.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chtChiFrecuencias)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvChi)).BeginInit();
            this.SuspendLayout();
            // 
            // initTab
            // 
            this.initTab.Controls.Add(this.tabAleatorios);
            this.initTab.Controls.Add(this.tabChiCuadrado);
            this.initTab.Location = new System.Drawing.Point(1, 1);
            this.initTab.Name = "initTab";
            this.initTab.SelectedIndex = 0;
            this.initTab.Size = new System.Drawing.Size(780, 552);
            this.initTab.TabIndex = 0;
            // 
            // tabAleatorios
            // 
            this.tabAleatorios.Controls.Add(this.chkMultiplicativo);
            this.tabAleatorios.Controls.Add(this.btnNuevoRandom);
            this.tabAleatorios.Controls.Add(this.dgvAleatorios);
            this.tabAleatorios.Controls.Add(this.btnLimpiar);
            this.tabAleatorios.Controls.Add(this.btnGenerar);
            this.tabAleatorios.Controls.Add(this.txtIncremento);
            this.tabAleatorios.Controls.Add(this.lblIncremento);
            this.tabAleatorios.Controls.Add(this.txtModulo);
            this.tabAleatorios.Controls.Add(this.lblModulo);
            this.tabAleatorios.Controls.Add(this.txtMultiplicador);
            this.tabAleatorios.Controls.Add(this.lblMultiplicador);
            this.tabAleatorios.Controls.Add(this.txtSemilla);
            this.tabAleatorios.Controls.Add(this.lblSemilla);
            this.tabAleatorios.Location = new System.Drawing.Point(4, 25);
            this.tabAleatorios.Name = "tabAleatorios";
            this.tabAleatorios.Padding = new System.Windows.Forms.Padding(3);
            this.tabAleatorios.Size = new System.Drawing.Size(772, 523);
            this.tabAleatorios.TabIndex = 0;
            this.tabAleatorios.Text = "Aleatorios";
            this.tabAleatorios.UseVisualStyleBackColor = true;
            // 
            // chkMultiplicativo
            // 
            this.chkMultiplicativo.AutoSize = true;
            this.chkMultiplicativo.Location = new System.Drawing.Point(160, 9);
            this.chkMultiplicativo.Name = "chkMultiplicativo";
            this.chkMultiplicativo.Size = new System.Drawing.Size(110, 21);
            this.chkMultiplicativo.TabIndex = 8;
            this.chkMultiplicativo.Text = "Multiplicativo";
            this.chkMultiplicativo.UseVisualStyleBackColor = true;
            this.chkMultiplicativo.CheckedChanged += new System.EventHandler(this.ChkMultiplicativo_CheckedChanged);
            // 
            // btnNuevoRandom
            // 
            this.btnNuevoRandom.Location = new System.Drawing.Point(662, 10);
            this.btnNuevoRandom.Name = "btnNuevoRandom";
            this.btnNuevoRandom.Size = new System.Drawing.Size(100, 23);
            this.btnNuevoRandom.TabIndex = 7;
            this.btnNuevoRandom.Text = "NUEVO";
            this.btnNuevoRandom.UseVisualStyleBackColor = true;
            this.btnNuevoRandom.Visible = false;
            this.btnNuevoRandom.Click += new System.EventHandler(this.BtnNuevoRandom_Click);
            // 
            // dgvAleatorios
            // 
            this.dgvAleatorios.AllowUserToAddRows = false;
            this.dgvAleatorios.AllowUserToDeleteRows = false;
            this.dgvAleatorios.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvAleatorios.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvAleatorios.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.clmIteracion,
            this.clmX,
            this.clmR});
            this.dgvAleatorios.Location = new System.Drawing.Point(160, 35);
            this.dgvAleatorios.Name = "dgvAleatorios";
            this.dgvAleatorios.ReadOnly = true;
            this.dgvAleatorios.RowHeadersWidth = 51;
            this.dgvAleatorios.RowTemplate.Height = 24;
            this.dgvAleatorios.Size = new System.Drawing.Size(602, 478);
            this.dgvAleatorios.TabIndex = 6;
            // 
            // clmIteracion
            // 
            this.clmIteracion.HeaderText = "#";
            this.clmIteracion.MinimumWidth = 6;
            this.clmIteracion.Name = "clmIteracion";
            this.clmIteracion.ReadOnly = true;
            // 
            // clmX
            // 
            this.clmX.HeaderText = "X(n)";
            this.clmX.MinimumWidth = 6;
            this.clmX.Name = "clmX";
            this.clmX.ReadOnly = true;
            // 
            // clmR
            // 
            this.clmR.HeaderText = "R(n)";
            this.clmR.MinimumWidth = 6;
            this.clmR.Name = "clmR";
            this.clmR.ReadOnly = true;
            // 
            // btnLimpiar
            // 
            this.btnLimpiar.Location = new System.Drawing.Point(10, 274);
            this.btnLimpiar.Name = "btnLimpiar";
            this.btnLimpiar.Size = new System.Drawing.Size(100, 23);
            this.btnLimpiar.TabIndex = 6;
            this.btnLimpiar.Text = "LIMPIAR";
            this.btnLimpiar.UseVisualStyleBackColor = true;
            this.btnLimpiar.Click += new System.EventHandler(this.BtnLimpiar_Click);
            // 
            // btnGenerar
            // 
            this.btnGenerar.Location = new System.Drawing.Point(10, 245);
            this.btnGenerar.Name = "btnGenerar";
            this.btnGenerar.Size = new System.Drawing.Size(100, 23);
            this.btnGenerar.TabIndex = 5;
            this.btnGenerar.Text = "GENERAR";
            this.btnGenerar.UseVisualStyleBackColor = true;
            this.btnGenerar.Click += new System.EventHandler(this.BtnGenerar_Click);
            // 
            // txtIncremento
            // 
            this.txtIncremento.Location = new System.Drawing.Point(10, 200);
            this.txtIncremento.MaxLength = 9;
            this.txtIncremento.Name = "txtIncremento";
            this.txtIncremento.Size = new System.Drawing.Size(100, 22);
            this.txtIncremento.TabIndex = 4;
            // 
            // lblIncremento
            // 
            this.lblIncremento.AutoSize = true;
            this.lblIncremento.Location = new System.Drawing.Point(15, 175);
            this.lblIncremento.Name = "lblIncremento";
            this.lblIncremento.Size = new System.Drawing.Size(103, 17);
            this.lblIncremento.TabIndex = 0;
            this.lblIncremento.Text = "Incremento (c):";
            // 
            // txtModulo
            // 
            this.txtModulo.Location = new System.Drawing.Point(10, 145);
            this.txtModulo.MaxLength = 9;
            this.txtModulo.Name = "txtModulo";
            this.txtModulo.Size = new System.Drawing.Size(100, 22);
            this.txtModulo.TabIndex = 3;
            // 
            // lblModulo
            // 
            this.lblModulo.AutoSize = true;
            this.lblModulo.Location = new System.Drawing.Point(15, 120);
            this.lblModulo.Name = "lblModulo";
            this.lblModulo.Size = new System.Drawing.Size(83, 17);
            this.lblModulo.TabIndex = 0;
            this.lblModulo.Text = "Modulo (m):";
            // 
            // txtMultiplicador
            // 
            this.txtMultiplicador.Location = new System.Drawing.Point(10, 90);
            this.txtMultiplicador.MaxLength = 9;
            this.txtMultiplicador.Name = "txtMultiplicador";
            this.txtMultiplicador.Size = new System.Drawing.Size(100, 22);
            this.txtMultiplicador.TabIndex = 2;
            // 
            // lblMultiplicador
            // 
            this.lblMultiplicador.AutoSize = true;
            this.lblMultiplicador.Location = new System.Drawing.Point(15, 65);
            this.lblMultiplicador.Name = "lblMultiplicador";
            this.lblMultiplicador.Size = new System.Drawing.Size(113, 17);
            this.lblMultiplicador.TabIndex = 0;
            this.lblMultiplicador.Text = "Multiplicador (a):";
            // 
            // txtSemilla
            // 
            this.txtSemilla.Location = new System.Drawing.Point(10, 35);
            this.txtSemilla.MaxLength = 9;
            this.txtSemilla.Name = "txtSemilla";
            this.txtSemilla.Size = new System.Drawing.Size(100, 22);
            this.txtSemilla.TabIndex = 1;
            // 
            // lblSemilla
            // 
            this.lblSemilla.AutoSize = true;
            this.lblSemilla.Location = new System.Drawing.Point(15, 10);
            this.lblSemilla.Name = "lblSemilla";
            this.lblSemilla.Size = new System.Drawing.Size(57, 17);
            this.lblSemilla.TabIndex = 0;
            this.lblSemilla.Text = "Semilla:";
            // 
            // tabChiCuadrado
            // 
            this.tabChiCuadrado.Controls.Add(this.chkExportar);
            this.tabChiCuadrado.Controls.Add(this.chkMixto);
            this.tabChiCuadrado.Controls.Add(this.lblSumatoriaResultado);
            this.tabChiCuadrado.Controls.Add(this.lblSumatoria);
            this.tabChiCuadrado.Controls.Add(this.chtChiFrecuencias);
            this.tabChiCuadrado.Controls.Add(this.dgvChi);
            this.tabChiCuadrado.Controls.Add(this.btnGenerarChi);
            this.tabChiCuadrado.Controls.Add(this.txtNumeros);
            this.tabChiCuadrado.Controls.Add(this.lblNumeros);
            this.tabChiCuadrado.Controls.Add(this.txtIntervalos);
            this.tabChiCuadrado.Controls.Add(this.lblIntervalos);
            this.tabChiCuadrado.Location = new System.Drawing.Point(4, 25);
            this.tabChiCuadrado.Name = "tabChiCuadrado";
            this.tabChiCuadrado.Padding = new System.Windows.Forms.Padding(3);
            this.tabChiCuadrado.Size = new System.Drawing.Size(772, 523);
            this.tabChiCuadrado.TabIndex = 1;
            this.tabChiCuadrado.Text = "Chi^2";
            this.tabChiCuadrado.UseVisualStyleBackColor = true;
            this.tabChiCuadrado.Enter += new System.EventHandler(this.TabChiCuadrado_Enter);
            // 
            // lblSumatoriaResultado
            // 
            this.lblSumatoriaResultado.AutoSize = true;
            this.lblSumatoriaResultado.Location = new System.Drawing.Point(711, 35);
            this.lblSumatoriaResultado.Name = "lblSumatoriaResultado";
            this.lblSumatoriaResultado.Size = new System.Drawing.Size(44, 17);
            this.lblSumatoriaResultado.TabIndex = 6;
            this.lblSumatoriaResultado.Text = "0.000";
            // 
            // lblSumatoria
            // 
            this.lblSumatoria.AutoSize = true;
            this.lblSumatoria.Location = new System.Drawing.Point(618, 35);
            this.lblSumatoria.Name = "lblSumatoria";
            this.lblSumatoria.Size = new System.Drawing.Size(100, 17);
            this.lblSumatoria.TabIndex = 5;
            this.lblSumatoria.Text = "Sumatoria Chi:";
            // 
            // chtChiFrecuencias
            // 
            this.chtChiFrecuencias.BorderlineColor = System.Drawing.Color.Black;
            this.chtChiFrecuencias.BorderlineDashStyle = System.Windows.Forms.DataVisualization.Charting.ChartDashStyle.Dash;
            chartArea5.Name = "ChartArea1";
            this.chtChiFrecuencias.ChartAreas.Add(chartArea5);
            legend5.Name = "LGND";
            legend5.Title = "Leyenda";
            this.chtChiFrecuencias.Legends.Add(legend5);
            this.chtChiFrecuencias.Location = new System.Drawing.Point(10, 249);
            this.chtChiFrecuencias.Name = "chtChiFrecuencias";
            this.chtChiFrecuencias.Palette = System.Windows.Forms.DataVisualization.Charting.ChartColorPalette.Pastel;
            series9.ChartArea = "ChartArea1";
            series9.Legend = "LGND";
            series9.Name = "FE";
            series10.ChartArea = "ChartArea1";
            series10.Legend = "LGND";
            series10.Name = "FO";
            this.chtChiFrecuencias.Series.Add(series9);
            this.chtChiFrecuencias.Series.Add(series10);
            this.chtChiFrecuencias.Size = new System.Drawing.Size(752, 266);
            this.chtChiFrecuencias.TabIndex = 4;
            title13.Docking = System.Windows.Forms.DataVisualization.Charting.Docking.Left;
            title13.Name = "valores";
            title13.Text = "Valores";
            title13.TextStyle = System.Windows.Forms.DataVisualization.Charting.TextStyle.Shadow;
            title14.Docking = System.Windows.Forms.DataVisualization.Charting.Docking.Bottom;
            title14.Name = "intervalos";
            title14.Text = "Intervalos";
            title14.TextStyle = System.Windows.Forms.DataVisualization.Charting.TextStyle.Shadow;
            title15.Name = "titulo grafico";
            title15.Text = "Grafico de frecuencias";
            this.chtChiFrecuencias.Titles.Add(title13);
            this.chtChiFrecuencias.Titles.Add(title14);
            this.chtChiFrecuencias.Titles.Add(title15);
            // 
            // dgvChi
            // 
            this.dgvChi.AllowUserToAddRows = false;
            this.dgvChi.AllowUserToDeleteRows = false;
            this.dgvChi.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle5.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle5.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle5.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvChi.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle5;
            this.dgvChi.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvChi.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.idRow,
            this.interval,
            this.obs_freq,
            this.exp_freq,
            this.chi_param});
            this.dgvChi.Location = new System.Drawing.Point(160, 35);
            this.dgvChi.Name = "dgvChi";
            this.dgvChi.ReadOnly = true;
            this.dgvChi.RowHeadersVisible = false;
            this.dgvChi.RowHeadersWidth = 51;
            this.dgvChi.RowTemplate.Height = 24;
            this.dgvChi.Size = new System.Drawing.Size(452, 208);
            this.dgvChi.TabIndex = 3;
            // 
            // idRow
            // 
            this.idRow.HeaderText = "#";
            this.idRow.MinimumWidth = 6;
            this.idRow.Name = "idRow";
            this.idRow.ReadOnly = true;
            // 
            // interval
            // 
            this.interval.HeaderText = "Interval";
            this.interval.MinimumWidth = 6;
            this.interval.Name = "interval";
            this.interval.ReadOnly = true;
            // 
            // obs_freq
            // 
            this.obs_freq.HeaderText = "Frec. Observada";
            this.obs_freq.MinimumWidth = 6;
            this.obs_freq.Name = "obs_freq";
            this.obs_freq.ReadOnly = true;
            // 
            // exp_freq
            // 
            this.exp_freq.HeaderText = "Frec. Esperada";
            this.exp_freq.MinimumWidth = 6;
            this.exp_freq.Name = "exp_freq";
            this.exp_freq.ReadOnly = true;
            // 
            // chi_param
            // 
            this.chi_param.HeaderText = "Chi-Sqr";
            this.chi_param.MinimumWidth = 6;
            this.chi_param.Name = "chi_param";
            this.chi_param.ReadOnly = true;
            // 
            // btnGenerarChi
            // 
            this.btnGenerarChi.Location = new System.Drawing.Point(10, 127);
            this.btnGenerarChi.Name = "btnGenerarChi";
            this.btnGenerarChi.Size = new System.Drawing.Size(100, 23);
            this.btnGenerarChi.TabIndex = 2;
            this.btnGenerarChi.Text = "GENERAR";
            this.btnGenerarChi.UseVisualStyleBackColor = true;
            this.btnGenerarChi.Click += new System.EventHandler(this.BtnGenerarChi_Click);
            // 
            // txtNumeros
            // 
            this.txtNumeros.Location = new System.Drawing.Point(10, 96);
            this.txtNumeros.MaxLength = 9;
            this.txtNumeros.Name = "txtNumeros";
            this.txtNumeros.Size = new System.Drawing.Size(100, 22);
            this.txtNumeros.TabIndex = 1;
            // 
            // lblNumeros
            // 
            this.lblNumeros.AutoSize = true;
            this.lblNumeros.Location = new System.Drawing.Point(7, 70);
            this.lblNumeros.Name = "lblNumeros";
            this.lblNumeros.Size = new System.Drawing.Size(69, 17);
            this.lblNumeros.TabIndex = 0;
            this.lblNumeros.Text = "Numeros:";
            // 
            // txtIntervalos
            // 
            this.txtIntervalos.Location = new System.Drawing.Point(10, 39);
            this.txtIntervalos.MaxLength = 2;
            this.txtIntervalos.Name = "txtIntervalos";
            this.txtIntervalos.Size = new System.Drawing.Size(100, 22);
            this.txtIntervalos.TabIndex = 1;
            // 
            // lblIntervalos
            // 
            this.lblIntervalos.AutoSize = true;
            this.lblIntervalos.Location = new System.Drawing.Point(7, 13);
            this.lblIntervalos.Name = "lblIntervalos";
            this.lblIntervalos.Size = new System.Drawing.Size(73, 17);
            this.lblIntervalos.TabIndex = 0;
            this.lblIntervalos.Text = "Intervalos:";
            // 
            // chkMixto
            // 
            this.chkMixto.AutoSize = true;
            this.chkMixto.Location = new System.Drawing.Point(160, 8);
            this.chkMixto.Name = "chkMixto";
            this.chkMixto.Size = new System.Drawing.Size(62, 21);
            this.chkMixto.TabIndex = 7;
            this.chkMixto.Text = "Mixto";
            this.chkMixto.UseVisualStyleBackColor = true;
            this.chkMixto.CheckedChanged += new System.EventHandler(this.ChkMixto_CheckedChanged);
            // 
            // chkExportar
            // 
            this.chkExportar.AutoSize = true;
            this.chkExportar.Location = new System.Drawing.Point(10, 156);
            this.chkExportar.Name = "chkExportar";
            this.chkExportar.Size = new System.Drawing.Size(124, 21);
            this.chkExportar.TabIndex = 7;
            this.chkExportar.Text = "Exportar salida";
            this.chkExportar.UseVisualStyleBackColor = true;
            this.chkExportar.CheckedChanged += new System.EventHandler(this.ChkExportar_CheckedChanged);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(782, 553);
            this.Controls.Add(this.initTab);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "MainForm";
            this.Text = "SIM Generador Aleatorios";
            this.Load += new System.EventHandler(this.load);
            this.initTab.ResumeLayout(false);
            this.tabAleatorios.ResumeLayout(false);
            this.tabAleatorios.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvAleatorios)).EndInit();
            this.tabChiCuadrado.ResumeLayout(false);
            this.tabChiCuadrado.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chtChiFrecuencias)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvChi)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl initTab;
        private System.Windows.Forms.TabPage tabAleatorios;
        private System.Windows.Forms.TabPage tabChiCuadrado;
        private System.Windows.Forms.Label lblSemilla;
        private System.Windows.Forms.TextBox txtIncremento;
        private System.Windows.Forms.Label lblIncremento;
        private System.Windows.Forms.TextBox txtModulo;
        private System.Windows.Forms.Label lblModulo;
        private System.Windows.Forms.TextBox txtMultiplicador;
        private System.Windows.Forms.Label lblMultiplicador;
        private System.Windows.Forms.TextBox txtSemilla;
        private System.Windows.Forms.Button btnGenerar;
        private System.Windows.Forms.DataGridView dgvAleatorios;
        private System.Windows.Forms.DataGridViewTextBoxColumn clmIteracion;
        private System.Windows.Forms.DataGridViewTextBoxColumn clmX;
        private System.Windows.Forms.DataGridViewTextBoxColumn clmR;
        private System.Windows.Forms.Button btnNuevoRandom;
        private System.Windows.Forms.CheckBox chkMultiplicativo;
        private System.Windows.Forms.Button btnLimpiar;
        private System.Windows.Forms.Button btnGenerarChi;
        private System.Windows.Forms.TextBox txtNumeros;
        private System.Windows.Forms.Label lblNumeros;
        private System.Windows.Forms.TextBox txtIntervalos;
        private System.Windows.Forms.Label lblIntervalos;
        private System.Windows.Forms.Label lblSumatoriaResultado;
        private System.Windows.Forms.Label lblSumatoria;
        private System.Windows.Forms.DataVisualization.Charting.Chart chtChiFrecuencias;
        private System.Windows.Forms.DataGridView dgvChi;
        private System.Windows.Forms.DataGridViewTextBoxColumn idRow;
        private System.Windows.Forms.DataGridViewTextBoxColumn interval;
        private System.Windows.Forms.DataGridViewTextBoxColumn obs_freq;
        private System.Windows.Forms.DataGridViewTextBoxColumn exp_freq;
        private System.Windows.Forms.DataGridViewTextBoxColumn chi_param;
        private System.Windows.Forms.CheckBox chkMixto;
        private System.Windows.Forms.CheckBox chkExportar;
    }
}


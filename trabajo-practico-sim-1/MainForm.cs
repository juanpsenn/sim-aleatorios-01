﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Windows.Forms;
using trabajo_practico_sim_1.modules.generator;
using trabajo_practico_sim_1.modules;
using trabajo_practico_sim_1.modules.pagination;
using System.Linq;
using System.IO;

namespace trabajo_practico_sim_1
{
    public partial class MainForm : Form
    {

        private Generator rng = new Generator();
        private Dictionary<string, string> values = new Dictionary<string, string>();
        private Dictionary<string, string> chi_values = new Dictionary<string, string>();
        private List<Interval> intervals;

        private bool generated = false;

        public MainForm()
        {
            InitializeComponent();
        }

        private void load(object sender, EventArgs e)
        {
        }

        private void FillDataGridViewChi(IEnumerable<double> list_randoms)
        {
            if (chkExportar.Checked)
            {
                this.SetFrequenciesAndExport(list_randoms);
            }
            else
            {
                this.SetFrequencies(list_randoms);
            }

            double chi_sum = 0;
            foreach (Interval i in this.intervals)
            {
                chi_sum += (double)i.chi_squared;
                dgvChi.Rows.Add(this.intervals.IndexOf(i), i.GetIntervalBounds(),
                    i.observed_freq.ToString("0.0"), i.expected_freq.ToString("0.0"),
                    i.chi_squared.ToString("0.0000"));
                chtChiFrecuencias.Series["FE"].Points.AddXY(i.GetIntervalBounds(), i.expected_freq);
                chtChiFrecuencias.Series["FO"].Points.AddXY(i.GetIntervalBounds(), i.observed_freq);

            }

            lblSumatoriaResultado.Text = chi_sum.ToString("0.0000");
        }

        private void SetFrequenciesAndExport(IEnumerable<double> list_randoms)
        {
            TextWriter tw = new StreamWriter("list.txt");
            var counter = 1;
            foreach (float num in list_randoms)
            {
                //Export numbers to .txt file
                tw.Write(num.ToString("0.0000") + ", ");
                if (counter % 11 == 0)
                {
                    tw.Write("\n");
                }
                counter++;

                //Find the interval
                foreach (Interval i in intervals)
                {
                    if (num <= i.upper_bound && num >= i.lower_bound)
                    {
                        i.observed_freq++;
                        break;
                    }
                }
            }

            //Set chi-squared for each interval
            foreach (Interval i in intervals)
            {
                i.chi_squared = (float)(Math.Pow((double)(i.observed_freq - i.expected_freq), 2) / i.expected_freq);
            }
            tw.Close();
        }

        private void SetFrequencies(IEnumerable<double> list_randoms)
        {

            foreach (float num in list_randoms)
            {
                //Find the interval
                foreach (Interval i in intervals)
                {
                    if (num <= i.upper_bound && num >= i.lower_bound)
                    {
                        i.observed_freq++;
                        break;
                    }
                }
            }

            //Set chi-squared for each interval
            foreach (Interval i in intervals)
            {
                i.chi_squared = (float)(Math.Pow((double)(i.observed_freq - i.expected_freq), 2) / i.expected_freq);
            }
        }

        public List<Interval> SetIntervals(float max, float min, int int_q, float paso, int n)
        {
            List<Interval> intervals = new List<Interval>();
            Interval aux = new Interval();

            //Set first interval
            aux.lower_bound = (float)Math.Round(min, 4);
            aux.upper_bound = (float)Math.Round(min + paso, 4);
            aux.expected_freq = (float)Math.Round((double)n / int_q, 4);
            intervals.Add(aux);

            //Set the following intervals from 1 to intervals quantity
            for (int i = 1; i < int_q; i++)
            {
                aux = new Interval();
                aux.lower_bound = (float)Math.Round(intervals[intervals.Count - 1].upper_bound, 4);
                aux.upper_bound = (float)Math.Round(aux.lower_bound + paso, 4);
                aux.expected_freq = (float)n / (float)int_q;
                intervals.Add(aux);
            }

            return intervals;
        }

        private void BtnLimpiar_Click(object sender, EventArgs e)
        {
            this.ResetGenerator();
        }

        private void BtnGenerar_Click(object sender, EventArgs e)
        {
            values.Clear();
            dgvAleatorios.Rows.Clear();
            values.Add("seed", txtSemilla.Text);
            values.Add("a", txtMultiplicador.Text);
            values.Add("m", txtModulo.Text);
            values.Add("c", txtIncremento.Text);

            if (DataFilter.ValidateDataInput(values))
            {
                this.generated = true;
                btnNuevoRandom.Show();
                this.BlockInputs();



                rng.SetVariables(long.Parse(values["seed"]), long.Parse(values["m"]),
                    long.Parse(values["a"]), long.Parse(values["c"]));

                IEnumerable<long[]> randomNumbers = rng.GetSet(20);

                foreach (long[] randoms in randomNumbers.Reverse())
                {
                    decimal r = (decimal)randoms[1] / decimal.Parse(values["m"]);
                    dgvAleatorios.Rows.Add(randoms[0], randoms[1], r.ToString("0.0000"));
                };
                dgvAleatorios.Rows.Add("0", long.Parse(values["seed"]), (decimal.Parse(values["seed"]) / decimal.Parse(values["m"])).ToString("0.0000"));
            }
        }

        private void BtnNuevoRandom_Click(object sender, EventArgs e)
        {
            if (this.generated)
            {
                int rowCount = dgvAleatorios.Rows.Count;
                long nextNumber = rng.Next();
                decimal r = (decimal)nextNumber / decimal.Parse(values["m"]);
                dgvAleatorios.Rows.Insert(0, new string[] { rowCount.ToString(), nextNumber.ToString(), r.ToString("0.0000") });
            }
        }

        private void ChkMultiplicativo_CheckedChanged(object sender, EventArgs e)
        {
            if (chkMultiplicativo.Checked)
            {
                this.ResetGenerator();
                values["c"] = "0";
                txtIncremento.Text = "0";
                txtIncremento.Enabled = false;
            }
            else
            {
                this.ResetGenerator();
                txtIncremento.Enabled = true;
            }
        }

        private void ResetGenerator()
        {
            this.rng = new Generator();
            this.EnableInputs();
            btnNuevoRandom.Hide();
            values.Clear();
            dgvAleatorios.Rows.Clear();
        }

        private void BlockInputs()
        {
            txtIncremento.Enabled = false;
            txtModulo.Enabled = false;
            txtMultiplicador.Enabled = false;
            txtSemilla.Enabled = false;
            btnGenerar.Enabled = false;

        }

        private void EnableInputs()
        {
            if (chkMultiplicativo.Checked)
            {
                txtModulo.Enabled = true;
                txtMultiplicador.Enabled = true;
                txtSemilla.Enabled = true;
                btnGenerar.Enabled = true;
            }
            else
            {
                txtIncremento.Enabled = true;
                txtModulo.Enabled = true;
                txtMultiplicador.Enabled = true;
                txtSemilla.Enabled = true;
                btnGenerar.Enabled = true;
            }
        }

        private void BtnGenerarChi_Click(object sender, EventArgs e)
        {
            this.ClearChiWindow();
            chi_values.Add("intervalos", txtIntervalos.Text);
            chi_values.Add("numeros", txtNumeros.Text);

            int result, intervals_q;
            int.TryParse(txtNumeros.Text, out result);
            int.TryParse(txtIntervalos.Text, out intervals_q);

            if (DataFilter.ValidateDataInput(chi_values))
            {
                if (result <= 50000000 && intervals_q > 0 && intervals_q <= 20)
                {
                    IEnumerable<double> list_random;
                    if (chkMixto.Checked)
                    {
                        list_random = this.rng.GetRandoms((long)result);
                    }
                    else
                    {
                        Random rand = new Random();
                        list_random = Enumerable.Range(0, int.Parse(chi_values["numeros"])).Select(x => rand.NextDouble());
                    }

                    float[] listR = { 0.00f, 0.75f, 0.63f, 0.20f, 0.34f, 0.99f, 0.91f, 0.33f,
                        0.87f, 0.79f, 0.89f, 0.02f, 0.85f, 0.05f, 0.29f, 0.99f, 0.22f, 0.19f,
                        0.30f, 0.01f, 0.21f, 0.15f, 0.00f, 0.74f, 0.14f, 0.18f, 0.77f, 0.59f,
                        0.02f, 0.67f };


                    List<float> randoms = new List<float>(listR);

                    float paso = ((float)list_random.Max() - (float)list_random.Min()) / int.Parse(chi_values["intervalos"]);

                    intervals = this.SetIntervals((float)list_random.Max(), (float)list_random.Min(), int.Parse(chi_values["intervalos"]),
                        paso, int.Parse(chi_values["numeros"]));

                    this.FillDataGridViewChi(list_random);
                }
                else
                {
                    MessageBox.Show("Ingrese una cantidad de numeros menor a 50.000.000. \n" +
                        "Verifique que la cantidad de intervalos se encuentre en [1,20].");
                }
            }
        }

        private void TabChiCuadrado_Enter(object sender, EventArgs e)
        {
            this.ResetGenerator();
            long m = 4294967296;
            long a = 1664525;
            long c = 1013904223;
            this.rng.SetVariables(DateTime.Now.Ticks % m, m, a, c);
            this.ClearChiWindow();
        }

        private void ClearChiWindow()
        {
            dgvChi.Rows.Clear();
            chi_values.Clear();
            chtChiFrecuencias.Series["FE"].Points.Clear();
            chtChiFrecuencias.Series["FO"].Points.Clear();
            lblSumatoriaResultado.Text = "0.0000";
        }

        private void ChkMixto_CheckedChanged(object sender, EventArgs e)
        {
            if (chkMixto.Checked)
            {
                MessageBox.Show("Para el GC. Mixto se utilizaron los siguientes parametros:\n" +
                    "seed: DateTime.Now.Ticks % m\n" +
                    "m: 4294967296\n" +
                    "a: 1664525\n" +
                    "c: 1013904223");
            }
        }

        private void ChkExportar_CheckedChanged(object sender, EventArgs e)
        {
            if (chkExportar.Checked)
            {
                MessageBox.Show("Los numeros generados se exportaran a un archivo list.txt " +
                "ubicado en el directorio del ejecutable.\n" +
                "Tenga en cuenta que mientras mas cantidad de numeros mayor sera el tamaño " +
                "del archivo generado.\n" +
                "En una prueba de 50 millones de numeros se obtuvieron 25 segundos de tiempo " +
                "de ejecucion y un archivo de 385MBs.");
            }
        }
    }
}
